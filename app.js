'use strict';
const kafka = require('kafka-node');

module.exports = app => {
  app.beforeStart(async () => {
    const ctx = app.createAnonymousContext();
    console.log('--------------------------kafka-----------------------');
    const Producer = kafka.Producer; // 生产者
    const KeyedMessage = kafka.KeyedMessage;
    const client = new kafka.KafkaClient({
      kafkaHost: app.config.kafkaHost,
    });
    // const producer = new Producer(client, app.config.producerConfig);
    console.log('【producer】');
    const producer = new Producer(client);
    const topicsToCreate = [
      {
        topic: 'topic1',
        partitions: 1,
      },
      {
        topic: 'topic2',
        partitions: 2,
      },
    ];
    const km = new KeyedMessage('key', 'message');
    const payloads = [
      { topic: 'topic1', messages: 'hi', partition: 0 },
      { topic: 'topic2', messages: [ 'hello', 'world', km ] },
    ];
    producer.once('ready', function() {
    //   producer.send(payloads, function(err, data) {
    //     console.log('producer ready send:', data);
    //   });
      producer.createTopics(topicsToCreate, (err, result) => {
        console.log('createTopics result:', result);
      });
    });
    producer.on('error', function(err) {
      console.error('ERROR:[Producer]' + err);
    });
    // 将生产者挂载到app上
    app.producer = producer;
    console.log('【consumer】');
    const Consumer = kafka.Consumer;
    const consumer = new Consumer(client, app.config.consumerTopics, {
      autoCommit: false, // 关闭自动消费反馈
    });
    consumer.on('message', async function(message) {
      console.log('消费message:', message);
      try {
        await ctx.service.log.insert(JSON.parse(message.value));
        // 手动提交当前主题的偏移量，当使用者离开时应调用此方法
        consumer.commit((err, data) => {
          console.log('commit:', data);
        });
      } catch (error) {
        console.log('message-content:', message);
        consumer.commit((err, data) => {
          console.log('commit:', data);
        });
        console.error('ERROR: [GetMessage] ', message, error);
      }
    });
    consumer.on('error', function(err) {
      console.error('ERROR:[Consumer]' + err);
    });
    console.log('【admin】');
    const admin = new kafka.Admin(client);
    admin.listTopics((err, res) => {
    //   console.log('topics list:', res);
    //   console.log('metadata:', res[1].metadata);
    //   const topics = Object.keys(res[1].metadata);
    //   for (const topic of topics) {
    //     console.log('topic:', topic);
    //     const partitions = Object.keys(res[1].metadata[topic]);
    //     for (const partition of partitions) {
    //       const topicPartitionInfo = res[1].metadata[topic][partition];
    //       console.log(`topic-${topic}-partition-${partition}:`, topicPartitionInfo);
    //     }
    //   }
    });
    // const resource = {
    //   resourceType: admin.RESOURCE_TYPES.topic, // 'broker' or 'topic'
    //   resourceName: 'topic1',
    //   configNames: [], // specific config names, or empty array to return all,
    // };

    // const payload = {
    //   resources: [ resource ],
    //   includeSynonyms: false, // requires kafka 2.0+
    // };
    // admin.describeConfigs(payload, (err, res) => {
    //   console.log(JSON.stringify(res, null, 1));
    // });
    const partition = 0;
    const topic = 'topic1';
    const offset = new kafka.Offset(client);
    // offset.fetch([
    //   { topic, partition, time: Date.now(), maxNum: 1 },
    // ], function(err, data) {
    //   // data
    //   // { 't': { '0': [999] } }
    //   console.log(data);
    // });
    // offset.fetchLatestOffsets([ topic ], function(error, offsets) {
    //   if (error) {
    //     console.log(error);
    //   }
    //   console.log(offsets[topic][partition]);
    // });
  });
};
