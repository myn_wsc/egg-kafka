/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1583376073321_6024';
  config.topic = 'logAction';
  config.kafkaHost = 'localhost:9092';
  config.producerConfig = {
    // Partitioner type (default = 0, random = 1, cyclic = 2, keyed = 3, custom = 4), default 0
    partitionerType: 1,
  };
  config.consumerTopics = [
    { topic: 'topic1', partition: 0 },
    { topic: 'topic2', partition: 0 },
    { topic: 'topic2', partition: 1 },
  ];
  // add your middleware config here
  config.middleware = [];
  config.security = {
    csrf: {
      enable: false,
    },
  };
  config.mysql = {
    clients: {
      basic: {
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: '03251222yxn',
        database: 'basic',
      },
      log: {
        host: 'localhost',
        port: '3306',
        user: 'root',
        password: '03251222yxn',
        database: 'logs',
      },
    },
    default: {},
    app: true,
    agent: false,
  };
  // sequelize config
  config.sequelize = {
    dialect: 'mysql',
    database: 'logs',
    host: 'localhost',
    port: '3306',
    username: 'root',
    password: '03251222yxn',
    dialectOptions: {
      requestTimeout: 999999,
    },
    pool: {
      acquire: 999999,
    },
  };
  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };

  return {
    ...config,
    ...userConfig,
  };
};
