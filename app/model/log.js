'use strict';
module.exports = app => {
  const { STRING, INTEGER, DATE, TEXT } = app.Sequelize;
  const Log = app.model.define('log', {
    /**
         * UUID
         */
    id: { type: STRING(36), primaryKey: true },
    /**
         * 日志类型
         */
    type: STRING(100),
    /**
         * 优先等级(数字越高，优先级越高）
         */
    level: INTEGER,
    /**
         * 操作者
         */
    operator: STRING(50),
    /**
         * 日志内容
         */
    content: TEXT,
    /**
         * IP
         */
    ip: STRING(36),
    /**
         * 当前用户代理信息
         */
    user_agent: STRING(150),
    /**
         * 错误堆栈
         */
    error_stack: TEXT,
    /**
         * URL
         */
    url: STRING(255),
    /**
         * 请求对象
         */
    request: TEXT,
    /**
         * 响应对象
         */
    response: TEXT,
    /**
         * 创建时间
         */
    created_at: DATE,
    /**
         * 更新时间
         */
    updated_at: DATE,
  });
 
  return Log;
}