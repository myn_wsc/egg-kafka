'use strict'

const Controller = require('egg').Controller;

class LogController extends Controller {
    /**
     * kafka控制日志信息流
     */
    async notice() {
        const producer = this.ctx.app.producer;
        const requestBody = this.ctx.query;
        const backInfo = await this.ctx.service.log.send(producer, requestBody);
        this.ctx.body = backInfo;
    }
}

module.exports = LogController;
