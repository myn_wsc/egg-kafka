'use strict';

const Service = require('egg').Service;
const uuidv1 = require('uuid/v1');
class LogService extends Service {
  async send(producer, params) {
    const payloads = [
      {
        topic: 'topic1',
        partitions: 0,
        messages: JSON.stringify(params),
      },
      {
        topic: 'topic2',
        partitions: 0,
        messages: JSON.stringify(params),
      },
    ];
    console.log('payloads:', payloads);
    // await this.ctx.service.log.insert(params);
    producer.send(payloads, function(err, data) {
      console.log('send:', data);
    });
    return 'success';
  }
  async insert(message) {
    try {
      const logDB = this.ctx.app.mysql.get('log');
      const ip = this.ctx.ip;
      //   const Logs = this.ctx.model.Log.build({
      //     id: uuidv1(),
      //     type: message.type || '',
      //     level: message.level || 0,
      //     operator: message.operator || '',
      //     content: message.content || '',
      //     ip,
      //     user_agent: message.user_agent || '',
      //     error_stack: message.error_stack || '',
      //     url: message.url || '',
      //     request: message.request || '',
      //     response: message.response || '',
      //     created_at: new Date(),
      //     update_at: new Date(),
      //   });
      const result = await logDB.insert('logs', {
        id: uuidv1(),
        type: message.type ? message.type : '',
        // level: message.level || 0,
        // operator: message.operator || '',
        // content: message.content || '',
        ip,
        // user_agent: message.user_agent || '',
        // error_stack: message.error_stack || '',
        // url: message.url || '',
        // request: message.request || '',
        // response: message.response || '',
        // created_at: new Date(),
        // update_at: new Date(),
      });
      if (result.affectedRows === 1) {
        console.log(`SUCCESS:[Insert ${message.type}]`);
      } else {
        console.log('ERROR:[Insert DB]', result);
      }
    } catch (error) {
      console.error('ERROR:[INSERT]', message, error);
    }
  }
}

module.exports = LogService;
